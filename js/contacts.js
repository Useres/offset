const telSelector = document.querySelector("input[type='tel']");
const im = new Inputmask("+7 (999)-999-99-99");

im.mask(telSelector);

const validation = new JustValidate('.contacts__content-form', {
  errorLabelStyle: {
    color: '#D11616',
  },

})
validation.addField('#name', [
  {
    rule: 'required',
    errorMessage: 'Как вас зовут?',
  },
  {
    rule: 'minLength',
    value: 3,
    errorMessage: 'Не короче 3 символов',
  },
  {
    rule: 'customRegexp',
    value: /^[A-ZА-ЯЁ]+$/i,
    errorMessage: 'Недопустимый формат',
  },
  {
    rule: 'maxLength',
    value: 30,
    errorMessage: 'Слишком длинное имя',
  },
])
validation.addField('#tel', [
  {
    rule: 'required',
    errorMessage: 'Укажите ваш телефон',
  },
  {
    validator: (value) => {
      const phone = telSelector.inputmask.unmaskedvalue()
      console.log(phone)
      return Number(phone) && phone.length === 10;
    },
    errorMessage: 'Телефон не корректный!',
  },
]).onSuccess((event) => {
  console.log('Validation passes and form submitted', event);

  let formData = new FormData(event.target);

  console.log(...formData);

  let xhr = new XMLHttpRequest();

  xhr.onreadystatechange = function () {
    if (xhr.readyState === 4) {
      if (xhr.status === 200) {
        console.log('Отправлено');
      }
    }
  }

  xhr.open('POST', 'mail.php', true);
  xhr.send(formData);

  event.target.reset();
});
