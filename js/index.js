document.addEventListener('DOMContentLoaded', () => {

    const dropdownLink = document.querySelectorAll('.header__lower-content-nav-art-submenu');
    const dropdownSubmenu = document.querySelectorAll('.header__lower-content__nav__dropdown');
    const dropdownArrow = document.querySelectorAll('.header__lower-content-nav-art-submenu-arrow');
    const element = document.querySelector('#gallery__select');
    const gallarySlider = document.querySelector('.gallery__content-slayder-img-swiper');
    const eventsSlider = document.querySelector('.events__content-swiper-container');
    const projectsSlider = document.querySelector('.projects__content-swiper-container');

    document.querySelector('#burger').addEventListener('click', function () {
        document.querySelector('#menu').classList.add('is-active')
    });
    document.querySelector('#close').addEventListener('click', function () {
        document.querySelector('#menu').classList.remove('is-active')
    });
    document.querySelector('#icon').addEventListener('click', function () {
        document.querySelector('#search').classList.add('is-active')
        document.querySelector('#icon').classList.add('is-none')
    });
    document.querySelector('#cover').addEventListener('click', function () {
        document.querySelector('#search').classList.remove('is-active')
        document.querySelector('#icon').classList.remove('is-none')
    });

    const modal = document.querySelector('.overlay')
    const gallerswiperButton = document.querySelectorAll('.gallery__content-slayder-img-button')
    const modalClose = document.querySelector('.overlay__content-gallery-modal-button-close')
    gallerswiperButton.forEach((el) => {
        el.addEventListener('click', () => {
            modal.classList.add('is-active');
        })
    })
    modalClose.addEventListener('click', () => {
        modal.classList.remove('is-active');
    })

    dropdownLink.forEach((el, index) => {
        el.addEventListener('click', (event) => {
            event.preventDefault();

            const dropdownSubmenuClassList = dropdownSubmenu[index].classList;
            if (dropdownSubmenuClassList.contains('dropdown-visible')) {
                dropdownSubmenuClassList.remove('dropdown-visible')
                dropdownArrow[index].classList.remove('rotate-arrow')

            } else {
                dropdownSubmenu.forEach((el) => el.classList.remove('dropdown-visible'))
                dropdownSubmenuClassList.add('dropdown-visible')
                dropdownArrow.forEach((el) => el.classList.remove('rotate-arrow'))
                dropdownArrow[index].classList.add('rotate-arrow')
            }
        });
    });

    window.onclick = function (event) {
        if (!event.target.matches('.header__lower-content-nav-art-submenu')) {
            let dropdowns = document.getElementsByClassName('header__lower-content__nav__dropdown');
            let i;
            for (i = 0; i < dropdowns.length; i++) {
                let openDropdown = dropdowns[i];
                if (openDropdown.classList.contains('dropdown-visible')) {
                    openDropdown.classList.remove('dropdown-visible');
                }
            }
        }
        if (!event.target.matches('.header__lower-content-nav-art-submenu')) {
            let dropArrows = document.getElementsByClassName('header__lower-content-nav-art-submenu-arrow');
            let i;
            for (i = 0; i < dropArrows.length; i++) {
                let openDropdown = dropArrows[i];
                if (openDropdown.classList.contains('rotate-arrow')) {
                    openDropdown.classList.remove('rotate-arrow');
                }
            }
        }

        if (!event.target.matches('.header__lower-content__nav__dropdown')) {
            let dropArrows = document.getElementsByClassName('.header__lower-content__nav__dropdown');
            let i;
            for (i = 0; i < dropArrows.length; i++) {
                if (openDropdown.classList.contains('dropdown--is-active')) {
                    openDropdown.classList.remove('dropdown--is-active');
                }
            }
        }
    };

    const choices = new Choices(element, {
        searchEnabled: false,
        itemSelectText: '',
    });

    var gallarySwiper = new Swiper(gallarySlider, {
        direction: 'horizontal',
        loop: false,
        slidesPerView: 1,
        pagination: {
            el: '.gallery__content-slayder-img-swiper-pagination',
            type: 'fraction',
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },

        breakpoints: {
            320: {
                slidesPerView: 1,
                slidesPerGroup: 1,
                spaceBetween: 10,
            },
            560: {
                slidesPerView: 2,
                slidesPerGroup: 2,
                spaceBetween: 38,
            },
            667: {
                slidesPerView: 2,
                spaceBetween: 31,
                slidesPerGroup: 2,
            },
            970: {
                spaceBetween: 38,
                slidesPerView: 2,
                slidesPerColumn: 1,
                slidesPerGroup: 2,
            },
            1024: {
                spaceBetween: 32,
                slidesPerView: 2,
                slidesPerColumn: 1,
                slidesPerGroup: 2,
            },
            1400: {
                spaceBetween: 50,
                slidesPerView: 2,
                slidesPerColumn: 1,
                slidesPerGroup: 2,
            },
            1500: {
                spaceBetween: 20,
                slidesPerView: 3,
                slidesPerGroup: 3,
            }
        }
    });

    $(".accordion").accordion({
        heightStyle: "content",
        collapsible: true,
        active: false,
    });

    document.querySelectorAll('.tabs-nav__button__none').forEach(function (tabsBtn) {
        tabsBtn.addEventListener('click', function (e) {
            const path = e.currentTarget.dataset.path;
            document.querySelectorAll('.tabs-nav__button__none').forEach(function (btn) {
                btn.classList.remove('tabs-nav__button__none--active')
            });
            e.currentTarget.classList.add('tabs-nav__button__none--active');
            document.querySelectorAll('.tabs-item').forEach(function (tabsBtn) {
                tabsBtn.classList.remove('tabs-item--active')
            });
            document.querySelector(`[data-target="${path}"]`).classList.add('tabs-item--active');
        });
    });

    var eventsSwiper = new Swiper(eventsSlider, {

        navigation: {
            nextEl: '.events__content-slayder-button-next',
            prevEl: '.events__content-slayder-button-prev',
        },
        pagination: {
            el: ".swiper-pagination",
            clickable: true,
        },
        slidesPerView: 1,
        slidesPerGroup: 1,
        spaceBetween: 20,
        slidersPerColumnFill: 'row',
        breakpoints: {
            400: {
                spaceBetween: 40,
                slidesPerView: 1,
                slidesPerColumn: 1,
                slidesPerGroup: 1,
            },
            600: {
                spaceBetween: 42,
                slidesPerView: 2,
                slidesPerColumn: 1,
                slidesPerGroup: 2,
            },
            970: {
                spaceBetween: 34,
                slidesPerView: 2,
                slidesPerColumn: 1,
                slidesPerGroup: 2,
            },
            1024: {
                spaceBetween: 27,
                slidesPerView: 3,
                slidesPerColumn: 1,
                slidesPerGroup: 3,
            },
            1200: {
                spaceBetween: 34,
                slidesPerView: 3,
                slidesPerColumn: 1,
                slidesPerGroup: 3,
            },
            1330: {
                spaceBetween: 50,
                slidesPerView: 3,
                slidesPerColumn: 1,
                slidesPerGroup: 3,
            }
        }
    });

    var projectsSwiper = new Swiper(projectsSlider, {

        navigation: {
            nextEl: '.projects__content-slayder-button-next',
            prevEl: '.projects__content-slayder-button-rev',
        },
        slidesPerView: 1,
        slidesPerGroup: 1,
        spaceBetween: 10,
        breakpoints: {
            500: {
                spaceBetween: 36,
                slidesPerView: 1,
                slidesPerColumn: 1,
                slidesPerGroup: 1,
            },
            600: {
                spaceBetween: 33,
                slidesPerView: 2,
                slidesPerGroup: 2,
            },
            1024: {
                spaceBetween: 51,
                slidesPerView: 2,
                slidesPerColumn: 1,
                slidesPerGroup: 2,
            },
            1201: {
                spaceBetween: 55,
                slidesPerView: 3,
                slidesPerColumn: 1,
                slidesPerGroup: 3,
            },
            1500: {
                spaceBetween: 50,
                slidesPerView: 3,
                slidesPerColumn: 1,
                slidesPerGroup: 3,
            }
        }
    });
    ymaps.ready(init);

    function init() {
        var myMap = new ymaps.Map("map", {
            center: [55.75846806898367, 37.60108849999989],
            zoom: 17
        });
        var myPlacemark = new ymaps.Placemark([55.75846806898367, 37.60108849999989], {}, {
            iconLayout: 'default#image',
            iconImageHref: 'img/location.svg',
            iconImageSize: [20, 20],
            iconImageOffset: [-25, -42]
        });
        myMap.geoObjects.add(myPlacemark);
    };

})
